var w = 640
var h = 480
var colors = {
  empty: [128, 128, 128],
  dark: [16, 16, 16],
  light: [212, 212, 212],
  background: [144, 144, 144],
  highlight: [64, 64, 64, 30],
  sign: [0, 0, 0]
}
var sounds = {
  bool: true,
  mirror: null,
  enter: null,
  shorten: null,
  lengthen: null,
  move: null,
  quit: null,
  rotate: null,
  fail: null,
  restart: null,
  solved: null,
  music: null
}
var texts = {
  font: null,
  title: 'FLECT',
  menu: ['START', 'ABOUT'],
  about: 'FLECT is a kind of\ntile-matching puzzle\nvideo game. You can\ntransform the colored\ntiles along mirrors.\nWhen all the frames\nare filled with their\nmatching color the\nlevel is solved.'
}
var axis = {
  orientation: 0,
  x: 2,
  y: 2,
  length: 1,
  wrapped: null,
  array: [0, 0, 0, 0, 0, 0, 0]
}
var state = {
  menu: true,
  levels: false,
  about: false,
  play: false,
  entered: false,
  solved: false
}
var board = {
  size: 7,
  tile: 52,
  distance: 12,
  x1: function() {
    return w * 0.5 - 3 * (this.tile + this.distance)
  },
  y1: function() {
    return h * 0.5 - 3 * (this.tile + this.distance)
  },
  x2: function() {
    return w * 0.5 - 2 * (this.tile + this.distance)
  },
  y2: function() {
    return h * 0.5 - 2 * (this.tile + this.distance)
  },
  weight: 6
}
var count = {
  item: 0,
  selected: -1,
  moves: 0,
  enter: 0,
  main: [1, 1, 1, 1, 2, 1, 2, 2, 3, 2, 3, 3, 2, 4, 3, 3, 3, 3, 3, 4, 5, 3, 4, 4, 4],
}
var animation = {
  bool: false,
  axis: false,
  speed: 10,
  rate: 60,
  axisx1: (w - (board.size * board.tile + (board.size - 1) * board.distance)) * 0.25,
  axisy1: (h - (board.size * board.tile + (board.size - 1) * board.distance) + board.tile) * 0.5,
  axisx2: (w - (board.size * board.tile + (board.size - 1) * board.distance)) * 0.5 + 2 * (board.tile + board.distance) + board.tile * 0.5,
  axisy2: (h - (board.size * board.tile + (board.size - 1) * board.distance)) * 0.5 + 2 * (board.tile + board.distance) - board.distance * 0.5,
}
var levels = create2DArray(5, 5, null, false)
var solvedState = create2DArray(5, 5, 0, true)
var startArray = [
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 2, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0],
    [0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 1, 2, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 2],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 2, 0, 0, 0],
    [0, 0, 2, 1, 0, 0, 0],
    [0, 0, 1, 2, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0],
    [2, 0, 0, 0, 0, 0, 0],
    [2, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 2, 2],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 2, 2]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 2, 1, 0, 0],
    [0, 0, 2, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 2, 1, 0, 0, 0],
    [0, 0, 0, 0, 2, 2, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 2, 0, 0, 0],
    [0, 0, 0, 0, 2, 0, 0],
    [0, 0, 0, 0, 2, 0, 0],
    [0, 0, 2, 2, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0],
    [0, 0, 1, 2, 0, 0, 0],
    [0, 0, 1, 2, 2, 0, 0],
    [0, 1, 1, 0, 2, 0, 0],
    [0, 0, 0, 2, 2, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [2, 2, 0, 0, 0, 2, 2],
    [0, 2, 0, 0, 2, 0, 0],
    [0, 1, 0, 0, 1, 0, 0],
    [0, 0, 2, 2, 0, 0, 0],
    [0, 0, 2, 2, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [2, 1, 0, 0, 0, 1, 2]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 2, 0, 0],
    [0, 0, 0, 0, 2, 0, 0],
    [0, 0, 0, 0, 1, 0, 0]
  ],
  [
    [0, 1, 2, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 1, 2, 0, 0],
    [0, 0, 2, 2, 1, 0, 0],
    [0, 2, 1, 1, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 1, 2, 0, 0, 0],
    [0, 2, 2, 2, 0, 0, 0],
    [0, 2, 0, 1, 1, 1, 0],
    [0, 0, 0, 1, 0, 2, 0],
    [0, 0, 0, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 0],
    [0, 2, 2, 2, 1, 0, 0],
    [0, 1, 1, 2, 2, 0, 0],
    [0, 0, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 2, 2, 0, 0, 0]
  ],
  [
    [1, 0, 0, 0, 0, 0, 0],
    [1, 0, 2, 2, 0, 0, 0],
    [0, 0, 2, 2, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [2, 0, 0, 0, 0, 2, 0],
    [0, 0, 0, 0, 0, 0, 2],
    [2, 0, 0, 0, 0, 2, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 1, 2, 2, 0],
    [0, 0, 0, 0, 2, 1, 0]
  ],
  [
    [0, 0, 0, 0, 0, 2, 0],
    [0, 0, 0, 0, 2, 2, 0],
    [0, 0, 0, 1, 2, 2, 0],
    [0, 0, 1, 1, 2, 0, 0],
    [0, 0, 1, 1, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0],
    [0, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 2, 2],
    [0, 2, 0, 0, 0, 2, 0],
    [0, 2, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 1],
    [0, 1, 2, 2, 2, 0, 0],
    [0, 0, 2, 2, 2, 1, 1],
    [0, 0, 2, 2, 2, 1, 1],
    [0, 0, 0, 1, 1, 1, 1],
    [0, 0, 0, 1, 1, 1, 1]
  ],
  [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 0, 2, 1, 0, 2, 1],
    [1, 2, 0, 1, 2, 0, 1],
    [1, 1, 1, 0, 1, 1, 1],
    [1, 0, 2, 1, 0, 2, 1],
    [1, 2, 0, 1, 2, 0, 1],
    [1, 1, 1, 1, 1, 1, 1]
  ],
  [
    [1, 0, 0, 0, 0, 0, 2],
    [1, 0, 0, 0, 0, 0, 2],
    [1, 0, 0, 0, 0, 0, 2],
    [1, 0, 0, 0, 0, 0, 2],
    [1, 0, 0, 0, 0, 0, 2],
    [1, 0, 0, 0, 0, 0, 2],
    [1, 0, 0, 0, 0, 0, 2]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [2, 2, 2, 0, 1, 0, 0],
    [2, 2, 2, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 1, 0],
    [2, 0, 0, 1, 1, 1, 0],
    [2, 2, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 2, 0, 1, 1, 0],
    [1, 1, 1, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 1, 0],
    [0, 0, 2, 2, 2, 0, 0],
    [0, 0, 0, 2, 2, 0, 0],
    [0, 2, 0, 1, 0, 0, 0]
  ]
]
var finalArray = [
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 2, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 2, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 2, 0, 0, 0]
  ],
  [
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [2, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 2, 1, 0, 0, 0],
    [0, 0, 1, 2, 0, 0, 0],
    [0, 0, 2, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 2, 0, 0, 0],
    [0, 0, 0, 2, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 2, 2, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 2, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 2, 2, 0, 0],
    [0, 0, 0, 2, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 2, 2, 2, 0, 0],
    [0, 0, 2, 1, 2, 0, 0],
    [0, 0, 2, 2, 2, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 2, 0, 0, 0],
    [0, 0, 1, 2, 2, 0, 0],
    [0, 2, 1, 1, 1, 0, 0],
    [0, 2, 2, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 1, 2, 2, 1, 0, 0],
    [0, 2, 2, 2, 2, 0, 0],
    [0, 2, 2, 2, 2, 0, 0],
    [0, 1, 2, 2, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 2, 0, 0],
    [0, 0, 0, 0, 2, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 2, 1, 1, 0, 0],
    [0, 1, 1, 2, 2, 0, 0],
    [0, 2, 2, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 2, 2, 0, 0, 0],
    [0, 2, 0, 2, 0, 0, 0],
    [0, 2, 2, 1, 1, 1, 0],
    [0, 0, 0, 1, 0, 1, 0],
    [0, 0, 0, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 2, 2, 0, 0, 0],
    [0, 2, 0, 0, 1, 0, 0],
    [0, 2, 0, 0, 1, 0, 0],
    [0, 0, 1, 1, 1, 2, 0],
    [0, 0, 0, 0, 2, 2, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 2, 0, 0, 0, 0],
    [0, 2, 2, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 2, 0, 2, 0, 0],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 2, 0, 2, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 2, 2, 2, 0],
    [2, 1, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 2, 2, 0, 0, 0],
    [0, 0, 2, 2, 0, 0, 0],
    [0, 0, 1, 1, 2, 2, 0],
    [0, 0, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 1, 1, 0],
    [0, 2, 2, 0, 1, 1, 0],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 1, 0, 2, 2, 0],
    [0, 1, 0, 0, 0, 2, 0],
    [0, 0, 0, 0, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 1, 0, 0, 0, 0],
    [0, 1, 1, 2, 2, 0, 0],
    [0, 0, 2, 2, 2, 1, 1],
    [0, 0, 2, 2, 2, 1, 1],
    [0, 0, 0, 1, 1, 1, 1],
    [0, 0, 0, 1, 1, 1, 1]
  ],
  [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 1, 2, 2, 1],
    [1, 0, 0, 1, 2, 2, 1],
    [1, 1, 1, 0, 1, 1, 1],
    [1, 2, 2, 1, 0, 0, 1],
    [1, 2, 2, 1, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1]
  ],
  [
    [0, 0, 0, 0, 2, 1, 0],
    [1, 0, 0, 0, 2, 1, 2],
    [1, 0, 0, 0, 2, 1, 2],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 2, 1, 0],
    [0, 0, 0, 0, 2, 1, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 0, 2, 2, 0],
    [0, 0, 1, 0, 2, 2, 0],
    [0, 0, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 2, 0],
    [1, 1, 0, 2, 2, 2, 0],
    [1, 1, 0, 2, 0, 0, 0]
  ],
  [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 2, 2, 0, 1, 1, 0],
    [0, 0, 2, 0, 1, 1, 0],
    [0, 0, 2, 2, 0, 0, 0],
    [0, 2, 0, 0, 0, 1, 0],
    [0, 2, 0, 1, 1, 1, 0],
    [0, 2, 0, 1, 0, 0, 0]
  ]
]
var startArrayCopy = JSON.parse(JSON.stringify(startArray))
var gamer = [0, 0]

function preload() {
  texts.font = loadFont('font/pressstart2p.ttf')
  sounds.move = loadSound('sound/move.mp3')
  sounds.enter = loadSound('sound/enter.mp3')
  sounds.quit = loadSound('sound/quit.mp3')
  sounds.shorten = loadSound('sound/shorten.mp3')
  sounds.lengthen = loadSound('sound/lengthen.mp3')
  sounds.rotate = loadSound('sound/rotate.mp3')
  sounds.restart = loadSound('sound/restart.mp3')
  sounds.mirror = loadSound('sound/mirror.mp3')
  sounds.solved = loadSound('sound/solved.mp3')
  sounds.fail = loadSound('sound/fail.mp3')
}

function setup() {
  createCanvas(w, h)
}

function draw() {
  rectMode(CENTER)
  frameRate(animation.rate)
  colorMode(RGB, 255, 255, 255, 100)
  background(colors.background)
  textFont(texts.font)
  // MENU
  if (state.menu === true && state.levels === false && state.about === false && state.play === false) {
    textAlign(CENTER, CENTER)
    textSize(64)
    strokeWeight(24)
    stroke(colors.empty)
    noFill()
    text(texts.title, 330, 96)
    strokeWeight(12)
    stroke(colors.dark)
    fill(colors.light)
    text(texts.title, 330, 96)
    textSize(32)
    for (var i = 0; i < texts.menu.length; i++) {
      strokeWeight(board.weight * 2)
      if (mouseX >= 0 && mouseX <= 640 && mouseY >= 350 - (texts.menu.length - i) * 64 - 32 && mouseY <= 350 - (texts.menu.length - i) * 64 + 32) {
        count.item = i
        stroke(colors.light)
      } else {
        stroke(colors.empty)
      }
      fill(colors.dark)
      text(texts.menu[i], 320, 350 + 64 * (i - texts.menu.length) + 2)
    }
    textSize(20)
    fill(colors.dark)
    noStroke()
    text('AHARONOFF 2020', 320, 440)
  }
  // LEVELS
  if (state.menu === false && state.levels === true && state.about === false && state.play === false) {
    for (var i = 0; i < levels.length; i++) {
      for (var j = 0; j < levels[i].length; j++) {
        if (solvedState[i][j] === 0) {
          noStroke()
          fill(colors.empty)
          drawSquare(board.x2(), board.y2(), i, j, 1)
          fill(colors.dark)
          noStroke()
          textSize(14)
          text(levels[i][j] + 1, board.x2() + i * (board.tile + board.distance), board.y2() + j * (board.tile + board.distance))
        }
        if (solvedState[i][j] === 1) {
          noStroke()
          fill(colors.light)
          drawSquare(board.x2(), board.y2(), i, j, 1)
          fill(colors.dark)
          noStroke()
          textSize(14)
          text(levels[i][j] + 1, board.x2() + i * (board.tile + board.distance), board.y2() + j * (board.tile + board.distance))
        }
      }
    }
    if (mouseX >= 320 - 158 && mouseX <= 320 + 158 && mouseY >= 240 - 158 && mouseY <= 240 + 158) {
      noFill()
      strokeWeight(board.weight)
      stroke(colors.sign)
      rect(320 - 161.5 + Math.floor((mouseX - 320 + 161.5)/ 64) * 64 + 32, 240 - 161.5 + Math.floor((mouseY - 240 + 161.5) / 64) * 64 + 32, 52, 52)
    }
    if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 && mouseY <= 433 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(51, 433)
    rotate(Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.666)
    pop()
    push()
    noStroke()
    translate(51, 433)
    rotate(-Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.666)
    pop()
  }
  // ABOUT
  if (state.menu === false && state.levels === false && state.about === true && state.play === false) {
    textAlign(LEFT)
    textSize(20)
    fill(colors.dark)
    text(texts.about, 128, 232)
    if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 && mouseY <= 433 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(51, 433)
    rotate(Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.666)
    pop()
    push()
    noStroke()
    translate(51, 433)
    rotate(-Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.666)
    pop()
  }
  // current LEVEL
  if (state.menu === false && state.levels === false && state.about === false && state.play === true) {
    // rotate AXIS
    if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 - 32 && mouseY <= 47 + 32) {
      stroke(colors.light)
    } else {
      stroke(colors.sign)
    }
    push()
    strokeCap(SQUARE)
    strokeWeight(board.weight)
    noFill()
    translate(585, 47)
    arc(0, 0, (board.tile + board.distance) * 0.5, (board.tile + board.distance) * 0.5, Math.PI * 0.5, Math.PI * 2)
    pop()
    if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 - 32 && mouseY <= 47 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(596, 47)
    rotate(-Math.PI * 0.4)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.25)
    pop()
    push()
    noStroke()
    translate(606, 44)
    rotate(Math.PI * 0.2)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.25)
    pop()
    // move UP
    if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 64 - 32 && mouseY <= 47 + 64 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(589 + 10, 47 + 64)
    rotate(-Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.5)
    pop()
    push()
    noStroke()
    translate(589 - 10, 47 + 64)
    rotate(Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.5)
    pop()
    // move DOWN
    if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 2 * 64 - 32 && mouseY <= 47 + 2 * 64 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(589 - 10, 47 + 2 * 64)
    rotate(-Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.5)
    pop()
    push()
    noStroke()
    translate(589 + 10, 47 + 2 * 64)
    rotate(Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.5)
    pop()
    // move RIGHT
    if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 3 * 64 - 32 && mouseY <= 47 + 3 * 64 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(589, 47 + 3 * 64 - 10)
    rotate(-Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.5)
    pop()
    push()
    noStroke()
    translate(589, 47 + 3 * 64 + 10)
    rotate(Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.5)
    pop()
    // move LEFT
    if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 4 * 64 - 32 && mouseY <= 47 + 4 * 64 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(589, 47 + 4 * 64 + 10)
    rotate(-Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.5)
    pop()
    push()
    noStroke()
    translate(589, 47 + 4 * 64 - 10)
    rotate(Math.PI * 0.25)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.5)
    pop()
    // shorten AXIS
    if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 5 * 64 - 32 && mouseY <= 47 + 5 * 64 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(589, 47 + 5 * 64)
    rotate(Math.PI * 0.5)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.75)
    pop()
    // lengthen AXIS
    if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 6 * 64 - 32 && mouseY <= 47 + 6 * 64 + 32) {
      fill(colors.light)
    } else {
      fill(colors.sign)
    }
    push()
    noStroke()
    translate(589, 47 + 6 * 64)
    rotate(-Math.PI * 0.5)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.75)
    pop()
    push()
    noStroke()
    translate(589, 47 + 6 * 64)
    rect(0, 0, board.weight, (board.tile + board.distance) * 0.75)
    pop()
    if (count.moves === count.main[count.selected] && state.solved === true) {

    } else {
      // return to LEVELS
      if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 && mouseY <= 433 + 32) {
        fill(colors.light)
      } else {
        fill(colors.sign)
      }
      push()
      noStroke()
      translate(51, 433)
      rotate(Math.PI * 0.25)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.75)
      pop()
      push()
      noStroke()
      translate(51, 433)
      rotate(-Math.PI * 0.25)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.75)
      pop()
      // restart
      if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 47 - 32 + 5 * 64 && mouseY <= 47 + 32 + 5 * 64) {
        stroke(colors.light)
      } else {
        stroke(colors.sign)
      }
      if (count.moves >= count.main[count.selected] && state.solved === false) {
        if (frameCount % 64 < 32) {
          stroke(colors.sign)
        } else {
          stroke(colors.light)
        }
      }
      push()
      strokeCap(SQUARE)
      strokeWeight(board.weight)
      noFill()
      translate(51 + 4, 47 + 5 * 64)
      arc(0, 0, (board.tile + board.distance) * 0.5, (board.tile + board.distance) * 0.5, -Math.PI, Math.PI * 0.5)
      pop()
      if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 47 - 32 + 5 * 64 && mouseY <= 47 + 32 + 5 * 64) {
        fill(colors.light)
      } else {
        fill(colors.sign)
      }
      if (count.moves >= count.main[count.selected] && state.solved === false) {
        if (frameCount % 64 < 32) {
          fill(colors.sign)
        } else {
          fill(colors.light)
        }
      }
      push()
      noStroke()
      translate(51 - 7, 47 + 5 * 64)
      rotate(Math.PI * 0.4)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.25)
      pop()
      push()
      noStroke()
      translate(51 - 17, 44 + 5 * 64)
      rotate(-Math.PI * 0.2)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.25)
      pop()

    }
    // skip to next level (if solved)
    if (count.moves === count.main[count.selected] && state.solved === true) {
      if (frameCount % 64 < 32) {
        fill(colors.sign)
      } else {
        fill(colors.light)
      }
      push()
      noStroke()
      translate(51, 433 - 2 * 64)
      rotate(Math.PI * 0.5)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.666)
      pop()
      push()
      noStroke()
      translate(51 + 14, 433 - 2 * 64 + 6)
      rotate(Math.PI * 0.25)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.35)
      pop()
      push()
      noStroke()
      translate(51 + 14, 433 - 2 * 64 - 6)
      rotate(-Math.PI * 0.25)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.35)
      pop()

      push()
      noStroke()
      translate(51, 433 - 1 * 64)
      rotate(Math.PI * 0.5)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.666)
      pop()
      push()
      noStroke()
      translate(51 + 14, 433 - 1 * 64 + 6)
      rotate(Math.PI * 0.25)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.35)
      pop()
      push()
      noStroke()
      translate(51 + 14, 433 - 1 * 64 - 6)
      rotate(-Math.PI * 0.25)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.35)
      pop()

      push()
      noStroke()
      translate(51, 433)
      rotate(Math.PI * 0.5)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.666)
      pop()
      push()
      noStroke()
      translate(51 + 14, 433 + 6)
      rotate(Math.PI * 0.25)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.35)
      pop()
      push()
      noStroke()
      translate(51 + 14, 433 - 6)
      rotate(-Math.PI * 0.25)
      rect(0, 0, board.weight, (board.tile + board.distance) * 0.35)
      pop()
    } else {
      // mirror
      if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 - 2 * 64 && mouseY <= 433 + 32 - 2 * 64) {
        stroke(colors.light)
      } else {
        stroke(colors.sign)
      }
      push()
      strokeWeight(board.weight)
      noFill()
      translate(51, 433 - 2 * 64)
      rect(0, 0, (board.tile + board.distance) * 0.666, (board.tile + board.distance) * 0.666)
      pop()
    }
    fill(colors.highlight[0], colors.highlight[1], colors.highlight[2], 15)
    noStroke()
    rect(51, 240 + 2 * 64, 64, 64 * 3)
    rect(589, 240, 64, 64 * 7)

    if (animation.bool === false) {
      // draw axis
      if (count.moves < count.main[count.selected] && state.solved === false) {
        rectMode(CENTER)
        noStroke()
        fill(colors.sign)
        if (axis.orientation === 0) {
          fill(colors.sign)
          if (axis.wrapped !== 0) {
            rect(
              board.x1() + (axis.x + ((axis.length - axis.wrapped) * 0.5 - 0.5)) * (board.tile + board.distance),
              board.y1() + (axis.y - 0.5) * (board.tile + board.distance),
              (axis.length - axis.wrapped) * (board.tile + board.distance),
              board.weight
            )
            rect(
              board.x1() + (axis.wrapped * 0.5 - 0.5) * (board.tile + board.distance),
              board.y1() + (axis.y - 0.5) * (board.tile + board.distance),
              axis.wrapped * (board.tile + board.distance),
              board.weight
            )
            if (axis.y === 0) {
              rect(
                board.x1() + (axis.x + ((axis.length - axis.wrapped) * 0.5 - 0.5)) * (board.tile + board.distance),
                board.y1() + (axis.y + 6.5) * (board.tile + board.distance),
                (axis.length - axis.wrapped) * (board.tile + board.distance),
                board.weight
              )
              rect(
                board.x1() + (axis.wrapped * 0.5 - 0.5) * (board.tile + board.distance),
                board.y1() + (axis.y + 6.5) * (board.tile + board.distance),
                axis.wrapped * (board.tile + board.distance),
                board.weight
              )
            }
            fill(colors.highlight)
            rect(
              board.x1() + (axis.x + ((axis.length - axis.wrapped) * 0.5 - 0.5)) * (board.tile + board.distance),
              board.y1() + 3 * 64,
              (axis.length - axis.wrapped) * (board.tile + board.distance),
              7 * (board.tile + board.distance)
            )
            rect(
              board.x1() + (axis.wrapped * 0.5 - 0.5) * (board.tile + board.distance),
              board.y1() + 3 * 64,
              axis.wrapped * (board.tile + board.distance),
              7 * (board.tile + board.distance)
            )
          } else {
            rect(
              board.x1() + (axis.x + (axis.length * 0.5 - 0.5)) * (board.tile + board.distance),
              board.y1() + (axis.y - 0.5) * (board.tile + board.distance),
              axis.length * (board.tile + board.distance),
              board.weight
            )
            if (axis.y === 0) {
              fill(colors.sign)
              rect(
                board.x1() + (axis.x + (axis.length * 0.5 - 0.5)) * (board.tile + board.distance),
                board.y1() + (axis.y + 6.5) * (board.tile + board.distance),
                axis.length * (board.tile + board.distance),
                board.weight
              )
            }
            fill(colors.highlight)
            rect(
              board.x1() + (axis.x + (axis.length * 0.5 - 0.5)) * (board.tile + board.distance),
              board.y1() + 3 * 64,
              axis.length * (board.tile + board.distance),
              7 * (board.tile + board.distance)
            )
          }
        } else {
          if (axis.wrapped !== 0) {
            fill(colors.sign)
            rect(
              board.x1() + (axis.x - 0.5) * (board.tile + board.distance),
              board.y1() + (axis.y + ((axis.length - axis.wrapped) * 0.5 - 0.5)) * (board.tile + board.distance),
              board.weight,
              (axis.length - axis.wrapped) * (board.tile + board.distance)
            )
            rect(
              board.x1() + (axis.x - 0.5) * (board.tile + board.distance),
              board.y1() + (axis.wrapped * 0.5 - 0.5) * (board.tile + board.distance),
              board.weight,
              axis.wrapped * (board.tile + board.distance)
            )
            if (axis.x === 0) {
              rect(
                board.x1() + (axis.x + 6.5) * (board.tile + board.distance),
                board.y1() + (axis.y + ((axis.length - axis.wrapped) * 0.5 - 0.5)) * (board.tile + board.distance),
                board.weight,
                (axis.length - axis.wrapped) * (board.tile + board.distance)
              )
              rect(
                board.x1() + (axis.x + 6.5) * (board.tile + board.distance),
                board.y1() + (axis.wrapped * 0.5 - 0.5) * (board.tile + board.distance),
                board.weight,
                axis.wrapped * (board.tile + board.distance)
              )
            }
            fill(colors.highlight)
            rect(
              board.x1() + 3 * 64,
              board.y1() + (axis.y + ((axis.length - axis.wrapped) * 0.5 - 0.5)) * (board.tile + board.distance),
              7 * (board.tile + board.distance),
              (axis.length - axis.wrapped) * (board.tile + board.distance)
            )
            rect(
              board.x1() + 3 * 64,
              board.y1() + (axis.wrapped * 0.5 - 0.5) * (board.tile + board.distance),
              7 * (board.tile + board.distance),
              axis.wrapped * (board.tile + board.distance)
            )
          } else {
            fill(colors.sign)
            rect(
              board.x1() + (axis.x - 0.5) * (board.tile + board.distance),
              board.y1() + (axis.y + (axis.length * 0.5 - 0.5)) * (board.tile + board.distance),
              board.weight,
              axis.length * (board.tile + board.distance)
            )
            if (axis.x === 0) {
              rect(
                board.x1() + (axis.x + 6.5) * (board.tile + board.distance),
                board.y1() + (axis.y + (axis.length * 0.5 - 0.5)) * (board.tile + board.distance),
                board.weight,
                axis.length * (board.tile + board.distance)
              )
            }
            fill(colors.highlight)
            rect(
              board.x1() + 3 * 64,
              board.y1() + (axis.y + (axis.length * 0.5 - 0.5)) * (board.tile + board.distance),
              7 * (board.tile + board.distance),
              axis.length * (board.tile + board.distance)
            )
          }
        }
      }
    }
    finalState(finalArray, count.selected)
    currentState(boardArray, finalArray[count.selected])
    // counter
    if (animation.bool === false) {
      noStroke()
      fill(colors.sign)
      if (count.moves < count.main[count.selected] && state.solved === false) {
        for (var i = 0; i < count.main[count.selected] - count.moves - 1; i++) {
          rect(animation.axisx1, animation.axisy1 + i * (board.tile + board.distance), (board.tile + board.distance), board.weight)
        }
      }
    }

    // animation
    fill(colors.sign)
    if (animation.bool === true && frameCount < ((animation.axisx2 - animation.axisx1) / animation.speed) && state.solved === false && count.moves < count.main[count.selected]) {
      if (animation.axis === true) {
        rect(animation.axisx1 + animation.speed * frameCount, animation.axisy1 + animation.speed * frameCount * ((animation.axisy2 - animation.axisy1) / (animation.axisx2 - animation.axisx1)), board.tile + board.distance, board.weight)
        for (var i = 0; i < count.main[count.selected] - count.moves - 1; i++) {
          rect(animation.axisx1, animation.axisy1 + board.tile + board.distance - frameCount * (1 / ((animation.axisx2 - animation.axisx1) / animation.speed)) * (board.tile + board.distance) + i * (board.tile + board.distance), board.tile + board.distance, board.weight)
        }
      }
    }
  }
}

function mousePressed() {
  if (animation.bool === false) {
    frameCount = 0
    // MENU navigation
    if (state.menu === true) {
      if (mouseX >= 0 && mouseX <= 640 && mouseY >= 350 - (texts.menu.length - 0) * 64 - 32 && mouseY <= 350 - (texts.menu.length - 0) * 64 + 32) {
        playSound(sounds.enter)
        setTimeout(function() {
          state.menu = false
          state.levels = true
          state.about = false
          creditState = false
          state.play = false
          setTimeout(function ready() {
            count.enter = 1
          }, 1)
        })
      } else if (mouseX >= 0 && mouseX <= 640 && mouseY >= 350 - (texts.menu.length - 1) * 64 - 32 && mouseY <= 350 - (texts.menu.length - 1) * 64 + 32) {
        playSound(sounds.enter)
        setTimeout(function() {
          state.menu = false
          state.levels = false
          state.about = true
          creditState = false
          state.play = false
        })
      }
    }
    // LEVELS navigation
    if (state.levels === true) {
      if (mouseX >= 320 - 158 && mouseX <= 320 + 158 && mouseY >= 240 - 158 && mouseY <= 240 + 158) {
        playSound(sounds.enter)
        setTimeout(function() {
          state.menu === false
          state.levels = false
          state.about = false
          creditState = false
          state.play = true
          animation.bool = true
          animation.axis = true
          setTimeout(function animationDelay() {
            animation.bool = false;
            animation.axis = false
          }, (20.5 / 60) * 1000)

          count.selected = levels[Math.floor((mouseX - 320 + 161.5)/ 64)][Math.floor((mouseY - 240 + 161.5) / 64)]

          boardArray = startArrayCopy[count.selected].map(function(arr) {
            return arr.slice()
          })
          axis.orientation = 0
          axis.x = 2
          axis.y = 2
          axis.length = 1
          count.moves = 0
          setTimeout(function levelEnter() {
            state.entered = true
          }, 1)
        })
      } else if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 && mouseY <= 433 + 32) {
        playSound(sounds.quit)
        state.menu = true
        state.levels = false
        state.about = false
        creditState = false
        state.play = false
        count.enter = 0
      }
    }
    // current LEVEL actions
    if (state.play === true) {
      if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 && mouseY <= 433 + 32) {
        setTimeout(function() {
          playSound(sounds.quit)
          state.menu = false
          state.levels = true
          state.about = false
          creditState = false
          state.play = false
          state.solved = false
          count.enter = 0
          count.moves = 0
          state.entered = false
          setTimeout(function ready() {
            count.enter = 1
          }, 1)
        })
      }
      if (count.moves < count.main[count.selected] && state.solved === false) {
        if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 - 32 && mouseY <= 47 + 32) {
          playSound(sounds.rotate)
          if (axis.orientation === 0) {
            axis.x += floor(axis.length * 0.5)
            axis.y -= floor(axis.length * 0.5)
            if (axis.y < 0) {
              axis.y = board.size + axis.y
            }
            if (axis.x >= board.size) {
              axis.x = axis.x - board.size
            }
          } else {
            axis.x -= floor(axis.length * 0.5)
            axis.y += floor(axis.length * 0.5)
            if (axis.x < 0) {
              axis.x = board.size + axis.x
            }
            if (axis.y >= board.size) {
              axis.y = axis.y - board.size
            }
          }
          axis.orientation = (axis.orientation + 1) % 2
        }
        if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 3 * 64 - 32 && mouseY <= 47 + 3 * 64 + 32) {
          playSound(sounds.move)
          axis.x++
          if (axis.x > board.size - 1) {
            axis.x = 0
          }
        }
        if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 2 * 64 - 32 && mouseY <= 47 + 2 * 64 + 32) {
          playSound(sounds.move)
          axis.y++
          if (axis.y > board.size - 1) {
            axis.y = 0
          }
        }
        if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 4 * 64 - 32 && mouseY <= 47 + 4 * 64 + 32) {
          playSound(sounds.move)
          axis.x--
          if (axis.x < 0) {
            axis.x = board.size - 1
          }
        }
        if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 64 - 32 && mouseY <= 47 + 64 + 32) {
          playSound(sounds.move)
          axis.y--
          if (axis.y < 0) {
            axis.y = board.size - 1
          }
        }
        if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 6 * 64 - 32 && mouseY <= 47 + 6 * 64 + 32) {
          playSound(sounds.lengthen)
          axis.length++
          if (axis.length > board.size) {
            axis.length = board.size
          }
        }
        if (mouseX >= 589 - 32 && mouseX <= 589 + 32 && mouseY >= 47 + 5 * 64 - 32 && mouseY <= 47 + 5 * 64 + 32) {
          playSound(sounds.shorten)
          axis.length--
          if (axis.length < 1) {
            axis.length = 1
          }
        }
        if (axis.orientation === 0) {
          if (axis.length + axis.x > board.size) {
            axis.wrapped = axis.length + axis.x - board.size
            for (var i = 0; i < board.size; i++) {
              if (i < axis.wrapped || i >= axis.x) {
                axis.array[i] = 1
              } else {
                axis.array[i] = 0
              }
            }
          } else {
            axis.wrapped = 0
            for (var i = 0; i < board.size; i++) {
              if (i >= axis.x && i < axis.x + axis.length) {
                axis.array[i] = 1
              } else {
                axis.array[i] = 0
              }
            }
          }
        } else {
          if (axis.length + axis.y > board.size) {
            axis.wrapped = axis.length + axis.y - board.size
            for (var i = 0; i < board.size; i++) {
              if (i < axis.wrapped || i >= axis.y) {
                axis.array[i] = 1
              } else {
                axis.array[i] = 0
              }
            }
          } else {
            axis.wrapped = 0
            for (var i = 0; i < board.size; i++) {
              if (i >= axis.y && i <= axis.y + axis.length - 1) {
                axis.array[i] = 1
              } else {
                axis.array[i] = 0
              }
            }
          }
        }
      } else {
        playSound(sounds.fail)
      }
      // skip to the next LEVEL
      if (count.moves === count.main[count.selected] && state.solved === true && mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 - 2 * 64 && mouseY <= 433 + 32 - 0 * 64) {
        for (var i = 0; i < levels.length; i++) {
          for (var j = 0; j < levels.length; j++) {
            if (levels[i][j] === count.selected) {
              if (count.moves <= count.main[count.selected]) {
                playSound(sounds.solved)
                solvedState[i][j] = 1
              }
            }
          }
        }
        for (var i = 0; i < levels.length; i++) {
          for (var j = 0; j < levels[i].length; j++) {
            if (count.selected === levels[i][j]) {
              gamer[0] = i
              gamer[1] = j
            }
          }
        }
        playSound(sounds.enter)
        setTimeout(function() {
          state.menu === false
          state.levels = false
          state.about = false
          creditState = false
          state.play = true
          animation.bool = true
          animation.axis = true
          setTimeout(function animationDelay() {
            animation.bool = false;
            animation.axis = false
          }, (20.5 / 60) * 1000)

          if (count.selected < 23) {
            count.selected++
          }
          boardArray = startArrayCopy[count.selected].map(function(arr) {
            return arr.slice()
          })
          axis.orientation = 0
          axis.x = 2
          axis.y = 2
          axis.length = 1
          count.moves = 0
          setTimeout(function levelEnter() {
            state.entered = true
          }, 1)
        })
        boardArray = startArray[count.selected].map(function(arr) {
          return arr.slice()
        })
        axis.orientation = 0
        axis.length = 1
        axis.x = 2
        axis.y = 2
        state.solved = false
        count.enter = 0
        count.moves = 0
        currAssignment = 0
        state.entered = true
        animation.bool = true
        animation.axis = true
        setTimeout(function animationDelay() {
          animation.bool = false;
          animation.axis = false
        }, (20.5 / 60) * 1000)
      } else {
        // mirror along axis
        if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 - 2 * 64 && mouseY <= 433 + 32 - 2 * 64 && state.entered === true && count.moves > count.main[count.selected] - 1) {
          playSound(sounds.fail)
        }
        if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 - 2 * 64 && mouseY <= 433 + 32 - 2 * 64 && state.entered === true && count.moves <= count.main[count.selected] - 1) {
          playSound(sounds.mirror)
          if (axis.orientation === 0) {
            for (var i = 0; i < board.size; i++) {
              shifting(boardArray[i], axis.y)
              if (axis.array[i] === 1) {
                boardArray[i].reverse()
              }
              shifting(boardArray[i], board.size - axis.y)
            }
          } else {
            shifting(boardArray, axis.x)
            boardArray = rotate2DArray(boardArray)
            for (var i = 0; i < board.size; i++) {
              if (axis.array[i] === 1) {
                boardArray[i].reverse()
              }
            }
            for (var i = 0; i < 3; i++) {
              boardArray = rotate2DArray(boardArray)
            }
            shifting(boardArray, board.size - axis.x)
          }
          count.moves++
          if (count.moves >= count.main[count.selected] && state.solved === false) {
            playSound(sounds.fail)
          }
          axis.x = 2
          axis.y = 2
          axis.length = 1
          axis.orientation = 0
          axis.wrapped = 0
          animation.bool = true
          animation.axis = true
          setTimeout(function animationDelay() {
            animation.bool = false;
            animation.axis = false
          }, (((animation.axisx2 - animation.axisx1) / animation.speed) / animation.rate) * 1000)
        }
      }
      // check if LEVEL is SOLVED
      solutionChecker(boardArray, finalArray, count.selected)
      // restart LEVEL
      if (mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 47 - 32 + 5 * 64 && mouseY <= 47 + 32 + 5 * 64 && state.solved === false) {
        playSound(sounds.restart)
        boardArray = startArray[count.selected].map(function(arr) {
          return arr.slice()
        })
        axis.orientation = 0
        axis.length = 1
        axis.x = 2
        axis.y = 2
        state.solved = false
        count.enter = 0
        count.moves = 0
        currAssignment = 0
        state.entered = true
        animation.bool = true
        animation.axis = true
        setTimeout(function animationDelay() {
          animation.bool = false;
          animation.axis = false
        }, (20.5 / 60) * 1000)
      }
    }
    // quit ABOUT
    if (state.about === true && mouseX >= 51 - 32 && mouseX <= 51 + 32 && mouseY >= 433 - 32 && mouseY <= 433 + 32) {
      playSound(sounds.quit)
      state.menu = true
      state.levels = false
      state.about = false
      creditState = false
      state.play = false
      count.enter = 0
    }
  }
}

function playSound(s) {
  if (sounds.bool === true) {
    s.play()
  }
}

function solutionChecker(array, final, selected) {
  if (JSON.stringify(array.flat()) === JSON.stringify(final[selected].flat())) {
    state.solved = true
  } else {
    state.solved = false
  }
}

function returnAfterSolved(c) {
  if (state.solved === true) {
    for (var i = 0; i < levels.length; i++) {
      for (var j = 0; j < levels.length; j++) {
        if (levels[i][j] === count.selected) {
          if (count.moves <= c[count.selected]) {
            playSound(sounds.solved)
            solvedState[i][j] = 1
          }
        }
      }
    }
    for (var i = 0; i < levels.length; i++) {
      for (var j = 0; j < levels[i].length; j++) {
        if (count.selected === levels[i][j]) {
          gamer[0] = i
          gamer[1] = j
        }
      }
    }
  }
}

function currentState(array, final) {
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      for (var k = 0; k < 3; k++) {
        if (array[i][j] === 0) {
          fill(colors.empty)
        }
        if (array[i][j] === 1) {
          fill(colors.dark)
        }
        if (array[i][j] === 2) {
          fill(colors.light)
        }
      }
      noStroke()
      drawSquare(board.x1(), board.y1(), i, j, 0.7)
      if (array[i][j] === 0 && final[i][j] === 0) {
        drawSquare(board.x1(), board.y1(), i, j, 0.85)
      }
      if (array[i][j] === 0 && final[i][j] !== 0) {
        fill(colors.background)
        drawSquare(board.x1(), board.y1(), i, j, 0.71)
      }
    }
  }
}

function finalState(array, level) {
  noStroke()
  for (var i = 0; i < array[level].length; i++) {
    for (var j = 0; j < array[level][i].length; j++) {
      if (array[level][i][j] === 1) {
        fill(colors.dark)
        drawSquare(board.x1(), board.y1(), i, j, 1)
      }
      if (array[level][i][j] === 2) {
        fill(colors.light)
        drawSquare(board.x1(), board.y1(), i, j, 1)
      }
    }
  }
}

function drawSquare(cx, cy, x, y, z) {
  rect(
    cx + x * (board.tile + board.distance),
    cy + y * (board.tile + board.distance),
    z * board.tile,
    z * board.tile
  )
}

function shifting(matrix, axis) {
  for (var i = 0; i < axis; i++) {
    matrix.push(matrix[0])
    matrix.shift(matrix[0])
  }
}

function mirror2DArray(matrix) {
  var result = []
  for (var i = 0; i < matrix.length; i++) {
    result[i] = matrix[i].slice()
  }
  for (var i = 0; i < result.length; i++) {
    result[i].reverse()
  }
  return result
}

function rotate2DArray(matrix) {
  var result = []
  for (var i = 0; i < matrix[0].length; i++) {
    var row = matrix.map(e => e[i]).reverse()
    result.push(row)
  }
  return result
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
